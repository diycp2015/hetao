#include "hetao_socgi.h"

#include "LOGC.h"

INITHTTPAPPLICATION InitHttpApplication ;
int InitHttpApplication( struct HttpApplicationContext *ctx )
{
	InfoLog( __FILE__ , __LINE__ , "InitHttpApplication" );
	
	return HTTP_OK;
}

REDIRECTHTTPDOMAIN RedirectHttpDomain ;
int RedirectHttpDomain( struct HttpApplicationContext *ctx )
{
	InfoLog( __FILE__ , __LINE__ , "RedirectHttpDomain" );
	
	return HTTP_OK;
}

REWRITEHTTPURI RewriteHttpUri ;
int RewriteHttpUri( struct HttpApplicationContext *ctx )
{
	InfoLog( __FILE__ , __LINE__ , "RewriteHttpUri" );
	
	return HTTP_OK;
}

BEFOREPROCESSHTTPRESOURCE BeforeProcessHttpResource ;
int BeforeProcessHttpResource( struct HttpApplicationContext *ctx )
{
	InfoLog( __FILE__ , __LINE__ , "BeforeProcessHttpResource" );
	
	return HTTP_OK;
}

PROCESSHTTPRESOURCE ProcessHttpResource ;
int ProcessHttpResource( struct HttpApplicationContext *ctx )
{
	InfoLog( __FILE__ , __LINE__ , "ProcessHttpResource" );
	
	return 0;
}

GETHTTPRESOURCE GetHttpResource ;
int GetHttpResource( struct HttpApplicationContext *ctx )
{
	char	http_body[ 1024 ] ;
	int	http_body_len ;
	
	int	nret = 0 ;
	
	InfoLog( __FILE__ , __LINE__ , "GetHttpResource" );
	
	memset( http_body , 0x00 , sizeof(http_body) );
	http_body_len = SNPRINTF( http_body , sizeof(http_body)-1 , "hello test_socgi_function.socgi , my config filename is [%s][%s]\n" , SOCGIGetCurrentPathname(ctx) , SOCGIGetConfigPathfilename(ctx) ) ;
	nret = SOCGIFormatHttpResponse( ctx , http_body , http_body_len , NULL ) ;
	if( nret )
	{
		ErrorLog( __FILE__ , __LINE__ , "SOCGIFormatHttpResponse failed[%d]" , nret );
		return HTTP_INTERNAL_SERVER_ERROR;
	}
	else
	{
		InfoLog( __FILE__ , __LINE__ , "SOCGIFormatHttpResponse ok" );
	}
	
	return HTTP_OK;
}

AFTERPROCESSHTTPRESOURCE AfterProcessHttpResource ;
int AfterProcessHttpResource( struct HttpApplicationContext *ctx )
{
	InfoLog( __FILE__ , __LINE__ , "AfterProcessHttpResource" );
	
	return HTTP_OK;
}

CLEANHTTPAPPLICATION CleanHttpApplication ;
int CleanHttpApplication( struct HttpApplicationContext *ctx )
{
	InfoLog( __FILE__ , __LINE__ , "CleanHttpApplication" );
	
	return HTTP_OK;
}

