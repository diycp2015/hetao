#include "test_socgi_rest_full.h"

static struct RestServiceConfig		g_RestServiceConfigArray[] = 
	{
		{ HTTP_METHOD_GET , "/" , GET_ } ,					/* curl "http://localhost/" */
		{ HTTP_METHOD_GET , "/path1" , GET_path1 } ,				/* curl "http://localhost/path1" */
		{ HTTP_METHOD_GET , "/path1/" , GET_path1_ } ,				/* curl "http://localhost/path1/" */
		{ HTTP_METHOD_GET , "/path1/path2" , GET_path1_path2 } ,		/* curl "http://localhost/path1/path2" */
		{ HTTP_METHOD_GET , "/path1/path2/" , GET_path1_path2_ } ,		/* curl "http://localhost/path1/path2/" */
		{ HTTP_METHOD_GET , "/path1/path2/file" , GET_path1_path2_file } ,	/* curl "http://localhost/path1/path2/file" */
		{ HTTP_METHOD_GET , "/path1/{}/file" , GET_path1_n_file } ,		/* curl "http://localhost/path1/123/file1" */
		{ HTTP_METHOD_GET , "/path1/path2/file1" , GET_path1_path2_file1__key1_value1 } ,		/* curl "http://localhost/path1/path2/file1?key1=value1" */
		{ HTTP_METHOD_GET , "/path1/path2/file2" , GET_path1_path2_file2__key1_value1__key2_value2 } ,	/* curl "http://localhost/path1/path2/file2?key1=value1&key2=value2" */
		{ HTTP_METHOD_GET , "/path1/path2/file3" , GET_path1_path2_file3__ } ,				/* curl "http://localhost/path1/path2/file3?" */
		{ HTTP_METHOD_GET , "/path1/path2/file4" , GET_path1_path2_file4__key1 } ,			/* curl "http://localhost/path1/path2/file4?key1" */
		{ HTTP_METHOD_GET , "/path1/path2/file5" , GET_path1_path2_file5__key1_ } ,			/* curl "http://localhost/path1/path2/file5?key1=" */
		{ HTTP_METHOD_GET , "/path1/path2/file6" , GET_path1_path2_file6__key1__ } ,			/* curl "http://localhost/path1/path2/file6?key1&" */
		{ HTTP_METHOD_GET , "/path1/path2/file7" , GET_path1_path2_file7__key1___ } ,			/* curl "http://localhost/path1/path2/file7?key1=&" */
		{ HTTP_METHOD_POST , "/path1/file" , POST_path1_file } ,		/* curl -X POST -d "this is a POST test for restserver" "http://localhost/path1/file" */
		{ HTTP_METHOD_PUT , "/path1/file" , PUT_path1_file } ,		/* curl -X PUT -d "this is a PUT test for restserver" "http://localhost/path1/file" */
		{ HTTP_METHOD_DELETE , "/path1/file" , DELETE_path1_file } ,	/* curl -X DELETE -d "this is a DELETE test for restserver" "http://localhost/path1/file" */
		{ "" , "" , NULL }
	} ;

INITHTTPAPPLICATION InitHttpApplication ;
int InitHttpApplication( struct HttpApplicationContext *ctx )
{
	struct RestServiceControler	*ctl = NULL ;
	
	InfoLog( __FILE__ , __LINE__ , "InitHttpApplication" );
	
	ctl = RESTCreateRestServiceControler( g_RestServiceConfigArray ) ;
	if( ctl == NULL )
	{
		ErrorLog( __FILE__ , __LINE__ , "RESTCreateRestServiceControler failed" );
		return HTTP_INTERNAL_SERVER_ERROR;
	}
	else
	{
		DebugLog( __FILE__ , __LINE__ , "RESTCreateRestServiceControler ok" );
	}
	
	SOCGISetUserData( ctx , ctl );
	
	return HTTP_OK;
}

CALLHTTPAPPLICATION CallHttpApplication ;
int CallHttpApplication( struct HttpApplicationContext *ctx )
{
	struct RestServiceControler	*ctl = NULL ;
	
	int				nret = 0 ;
	
	InfoLog( __FILE__ , __LINE__ , "CallHttpApplication" );
	
	ctl = SOCGIGetUserData( ctx ) ;
	if( ctl == NULL )
	{
		ErrorLog( __FILE__ , __LINE__ , "SOCGIGetUserData failed" );
		return HTTP_INTERNAL_SERVER_ERROR;
	}
	else
	{
		DebugLog( __FILE__ , __LINE__ , "SOCGIGetUserData ok" );
	}
	
	nret = RESTDispatchRestServiceControler( ctl , ctx ) ;
	if( nret )
	{
		ErrorLog( __FILE__ , __LINE__ , "RESTDispatchRestServiceControler failed[%d]" , nret );
		return HTTP_INTERNAL_SERVER_ERROR;
	}
	else
	{
		DebugLog( __FILE__ , __LINE__ , "RESTDispatchRestServiceControler ok" );
	}
	
	return HTTP_OK;
}

CLEANHTTPAPPLICATION CleanHttpApplication ;
int CleanHttpApplication( struct HttpApplicationContext *ctx )
{
	struct RestServiceControler	*ctl = NULL ;
	
	InfoLog( __FILE__ , __LINE__ , "CleanHttpApplication" );
	
	ctl = SOCGIGetUserData( ctx ) ;
	if( ctl == NULL )
	{
		ErrorLog( __FILE__ , __LINE__ , "SOCGIGetUserData failed" );
		return HTTP_INTERNAL_SERVER_ERROR;
	}
	else
	{
		DebugLog( __FILE__ , __LINE__ , "SOCGIGetUserData ok" );
	}
	
	RESTDestroyRestServiceControler( ctl );
	
	return HTTP_OK;
}

