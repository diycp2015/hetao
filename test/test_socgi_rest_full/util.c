#include "test_socgi_rest_full.h"

void TravelUriPathsAndQueries( struct RestServiceContext *ctx , char *buf , int buf_size , int *p_buf_len )
{
	char		*method = NULL ;
	int		method_len ;
	char		*uri = NULL ;
	int		uri_len ;
	
	int		uri_paths_count ;
	int		uri_path_index ;
	char		*uri_path = NULL ;
	int		uri_path_len ;
	
	int		queries_count ;
	int		query_index ;
	char		*key = NULL ;
	int		key_len ;
	char		*value = NULL ;
	int		value_len ;
	
	BUFNSTRCAT( buf , buf_size , (*p_buf_len) , "--- REST PUBLIC CHECK ---\n" ) ;
	
	method = RESTGetHttpMethodPtr( ctx , & method_len ) ;
	BUFNPRINTF( buf , buf_size , (*p_buf_len) , "         method[%d][%.*s]\n" , method_len , method_len,method )
	
	uri = RESTGetHttpUriPtr( ctx , & uri_len ) ;
	BUFNPRINTF( buf , buf_size , (*p_buf_len) , "            uri[%d][%.*s]\n" , uri_len , uri_len,uri )
	
	uri_paths_count = RESTGetHttpUriPathsCount( ctx ) ;
	BUFNPRINTF( buf , buf_size , (*p_buf_len) , "uri_paths_count[%d]\n" , uri_paths_count )
	for( uri_path_index = 1 ; uri_path_index <= uri_paths_count ; uri_path_index++ )
	{
		uri_path = RESTGetHttpUriPathPtr( ctx , uri_path_index , & uri_path_len );
	BUFNPRINTF( buf , buf_size , (*p_buf_len) , "       uri_path[%d][%.*s]\n" , uri_path_index , uri_path_len,uri_path )
	}
	
	queries_count = RESTGetHttpUriQueriesCount( ctx ) ;
	BUFNPRINTF( buf , buf_size , (*p_buf_len) , "  queries_count[%d]\n" , queries_count ) ;
	for( query_index = 1 ; query_index <= queries_count ; query_index++ )
	{
		key = RESTGetHttpUriQueryKeyPtr( ctx , query_index , & key_len ) ;
		value = RESTGetHttpUriQueryValuePtr( ctx , query_index , & value_len ) ;
	BUFNPRINTF( buf , buf_size , (*p_buf_len) , "          query[%d][%.*s][%.*s]\n" , query_index , key_len,key , value_len,value )
	}
	
	return;
}

